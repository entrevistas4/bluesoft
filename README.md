# Bluesoft: Desafio Desenvolvedor Java Backend

## E-MAIL SOBRE O DESAFIO
Olá Rodolpho,Tudo bem?

Nós da Bluesoft ficamos muito felizes com o seu interesse em participar do processo seletivo.

Muito obrigado por querer trabalhar com a gente!

##### Gostou da nossa Oportunidade? 
Quer saber como é a Rotina de um Desenvolvedor na Bluesoft ? [Clique aqui](https://www.youtube.com/watch?v=GnZmatQ2lIE)

Para dar continuidade no processo, é necessário que realize um Desafio Técnico, tudo bem??

Mas antes de ler as regras do Desafio, temos algumas dicas,  [Veja aqui!](https://youtu.be/URb0itCiB0g)

### O Desafio!!!

A proposta do desafio é conhecer suas habilidades de resolver problemas de negócio por meio de programação. Para isto, preparamos uma aplicação de pedidos de compra com todo o frontend já desenvolvido e funcionando.

Seu desafio será apenas implementar o backend e fazer a integração com front.

Para que possamos conhecer um pouco mais sobre o seu trabalho, e mais especificamente sobre as suas habilidades de resolver problemas, preparamos essa atividade bem simples, que poderá nos mostrar um pouquinho sobre a maneira que você organiza o seu código, a legibilidade dele, sobre seus conhecimentos básicos de MVC, ORM e banco de dados, se você utiliza bem Design Patterns, testes unitários, se faz TDD, etc.

1. Neste tem um repositório do Git com um projeto já iniciado. Você deve descompactar o arquivo e, como ele já é um repositório, crie uma branch com seu nome e commit suas alterações nela;
2. Os detalhes sobre o funcionamento do projeto, regras de negócio e nova feature que você deve implementar estão descritas no README do projeto;
3. Quando finalizar o projeto, execute o comando mvn clean, compacte a pasta e compartilhe um link Google Drive (no modo público) com o desafio para avaliação para esse e-mail ou envie e-mail para talentos@bluesoft.com.br;
4. Além do projeto em zip, mande também o seu perfil do Github e do linkedin ( Obs.: não enviar o projeto para o Github e pode informar esses dados na mesma pergunta onde irá compartilhar o arquivo).

### Você vai precisar conhecer:

* Java
* Hibernate ou JPA
* Spring MVC
* REST API
* Bancos de Dados (Relacional)
* Básico de Javascript
* GIT
* Maven

### Algumas dicas básicas:

* É essencial que o código tenha ao menos testes de unidade para a regra de negócio.
* Evite colocar regras de negócio no Controller, não vemos isto como uma boa prática.
* Aplique boas práticas de desenvolvimento, [veja Clean Code](https://www.youtube.com/watch?v=Pjoi_JxXAcM) e [veja princípios SOLID](https://www.youtube.com/watch?v=DmrHBUeo0NE)
* Seja objetivo no código, não precisamos que você mostre tudo que sabe mas que faça bem feito o que desenvolver.* Utilizar a versão do java já existente no projeto. Quando falamos no vídeo para usar java 8, é apenas para indicar a utilização de Optional, Lambda, Streams e afins. Não mudar a versão do java do projeto.

*Se tiver alguma dúvida, responda esse e-mail ou envie e-mail para [talentos@bluesoft.com.br](talentos@bluesoft.com.br)*

---

#### Antes de iniciar o projeto, leia com atenção todos os tópicos.

## Proposta do desafio:

A proposta deste desafio é conhecer suas habilidades de resolver problemas de negócio por meio de programação.
Estamos enviando para você um arquivo zip contendo um projeto de uma aplicação Java Web (Spring Boot, Hibernate e AngularJs) com o Frontend já desenvolvido.

Seu desafio será implementar o Backend e integrar com o Frontend.

## O problema

Você deve desenvolver um sistema para realizar **pedidos** a partir de uma lista de **produtos**, conforme a imagem:

![](https://bluesoft-sp.s3-sa-east-1.amazonaws.com/download/desafio-bluesoft/novo-pedido.png)

Os produtos **já estão cadastrados** e possuem 2 atributos: GTIN (código de barra) e nome.

Quando o usuário digitar a quantidade de cada produto e clicar no botão Enviar, o sistema deverá chamar a API https://egf1amcv33.execute-api.us-east-1.amazonaws.com/dev/produto/{gtin} informando o GTIN de cada produto. O retorno da API será uma lista de **fornecedores** e dentro de cada fornecedor uma lista de **preços** de acordo com a **quantidade mínima** para compra. Não é obrigatório informar a quantidade para todos os produtos.

Veja um exemplo do retorno da API externa para o GTIN 7894900011517:
```json
[
    {
        "nome": "Fornecedor 1",
        "cnpj": "56.918.868/0001-20",
        "precos": [
            {
                "preco": 6.89,
                "quantidade_minima": 1
            },
            {
                "preco": 5.89,
                "quantidade_minima": 10
            }
        ]
    },
    {
        "nome": "Fornecedor 2"
        "cnpj": "37.563.823/0001-35",
        "precos": [
            {
                "preco": 6.8,
                "quantidade_minima": 1
            },
            {
                "preco": 6,
                "quantidade_minima": 10
            }
        ],
    }
]
```

O sistema deve selecionar o melhor fornecedor para compra de cada produto considerando o menor preço que atenda a quantidade mínima de compra. Em seguida, deverá agrupar os produtos de um mesmo fornecedor e criar um pedido para cada um. O resultado será algo semelhante a imagem:

![](https://bluesoft-sp.s3-sa-east-1.amazonaws.com/download/desafio-bluesoft/pedido-criado-sucesso.png)

Caso nenhum fornecedor atenda a quantidade mínima de qualquer um dos produtos, o sistema deverá retornar uma mensagem informando o usuário e não deve criar nenhum pedido:

![](https://bluesoft-sp.s3-sa-east-1.amazonaws.com/download/desafio-bluesoft/pedido-erro.png)

O pedido deve ter um fornecedor e uma lista dos itens comprados. Caso o fornecedor retornado pela API ainda não esteja cadastrado, o sistema deverá incluí-lo para poder vincular com o pedido.

O sistema também deverá possuir uma rota para listar todos os pedidos criados até o momento.

## Integração com Frontend

Para integrar o backend com o frontend, troque as rotas no arquivo `src/main/resources/static/app/service/PedidoService.js` pelas rotas que você criou.

## Reforçando

- É essencial que o código tenha ao menos testes de unidade para a regra de negócio;
- Evite colocar regras de negócio no Controller, não vemos isto como uma boa prática;
- Procuramos seguir boas práticas de programação. Sendo assim, mesmo que seja uma aplicação simples, aplique todas as boas práticas que você usaria em um sistema real. Aplique [Clean Code](https://www.youtube.com/watch?v=Pjoi_JxXAcM) e princípios [SOLID](https://www.youtube.com/watch?v=DmrHBUeo0NE).
- Seja objetivo no código, não precisamos que você mostre tudo que sabe mas que faça bem feito o que desenvolver.
